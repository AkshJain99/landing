from django.apps import AppConfig


class TryOnlineConfig(AppConfig):
    name = 'try_online'
